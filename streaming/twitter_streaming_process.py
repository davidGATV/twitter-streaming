from tweepy import StreamListener
from tweepy.models import Status
from analyzer.twitter_analyzer import TwitterAnalyzer
from managers.elasticsearch_connector import ElasticsearchManager
from helper import global_variables as gv
from helper.twitter_utils import parser_tweet, convert_status_tweet_to_json, remove_url


# =========================================================================
# ------------------------- STREAMING TWITTER -----------------------------
# =========================================================================


class TwitterDataGathering:
    def __init__(self, es_index: {str} = None):
        if gv.logger is None:
            gv.init_logger_object()
        self.sentiment_model = gv.sentiment_model
        if es_index is None:
            self.es_index = gv.index_name
        else:
            self.es_index = es_index
        self.twitter_analyzer = TwitterAnalyzer()
        self.elasticsearch_connector = ElasticsearchManager(host=gv.es_host,
                                                            port=gv.es_port)
        self.set_up_analyzer()
        self.set_up_elasticsearch()

    def set_up_analyzer(self):
        try:
            # Select Model for Sentiment Analysis
            self.twitter_analyzer.select_model()

            # Retrieve subjectivity
            if self.twitter_analyzer.blobber_model is None:
                self.twitter_analyzer.set_up_blobber_model()
        except Exception as e:
            gv.logger.error(e)

    def set_up_elasticsearch(self):
        try:
            # Check Elasticsearch connection
            if not self.elasticsearch_connector.connection:
                self.elasticsearch_connector.connect()

            # Check index if it does not exist
            if self.es_index not in self.elasticsearch_connector.get_indexes():
                self.elasticsearch_connector.create_new_index(index=self.es_index,
                                                              body={})
        except Exception as e:
            gv.logger.error(e)

    def process_streaming_tweets(self, status: {Status}):
        response, source_data = {}, {}
        try:
            # 1. Convert to json
            tweet_json = convert_status_tweet_to_json(status)
            if "retweeted_status" not in tweet_json.keys():
                # 3. Parse data
                tweet_response = parser_tweet(tweet=tweet_json)
                source_data.update(tweet_response)
                tweet_text = remove_url(tweet_response["text"])

                # 4. Add Sentiment Analysis
                if gv.sentiment_model == "flair":
                    gv.logger.info("Sentiment Analysis using Flair!")
                    response_sent = self.twitter_analyzer.flair_sentiment_analysis(tweet_text=tweet_text)
                else:
                    gv.logger.info("Sentiment Analysis using Vader!")
                    response_sent = self.twitter_analyzer.vader_sentiment_analysis(tweet_text=tweet_text)

                # 5. Concatenate dictionary with the results
                source_data.update(response_sent)

                # 6. Save data into Elasticseach
                # 6.1 Generate uuid based on the Twitter ID
                gv.logger.info("Ingesting Tweet into Elasticsearch at index: %s", self.es_index)
                uuid = self.elasticsearch_connector.generate_uuid_from_string(data_uuid=[source_data["id"]])
                # 6.2 Bulk data
                res_es = self.elasticsearch_connector.bulk_data_into_index(index=self.es_index,
                                                                           uuid=uuid,
                                                                           source_data=source_data)
                gv.logger.info("Done!")

                # 7. Prepare response
                if res_es:
                    response["status"] = "Tweet ingested"
                    response["code"] = 200
                else:
                    response["status"] = "Tweet NOT ingested"
                    response["code"] = 400
            else:
                response["status"] = "Retweet NOT ingested"
                response["code"] = 202

        except Exception as e:
            gv.logger.error(e)
            response["status"] = str(e)
            response["code"] = 400
        return response


class TwitterStreamListener(StreamListener):
    def __init__(self, es_index: {str}):
        super().__init__()
        self.twitter_analyzer = TwitterDataGathering(es_index=es_index)
        print(self.twitter_analyzer.es_index)

    def on_status(self, status: {Status}):
        # Process Tweets
        self.process_tweet(status)

    def on_error(self, status_code):
        if status_code == 420:
            gv.logger.warning("Enhance Your Calm; The App is Being Rate Limited For Making Too Many Requests!\n")
            return True
        else:
            gv.logger.error("Error %s when ingesting a tweet\n", str(status_code))
            return True

    def process_tweet(self, status: {Status}):
        response = self.twitter_analyzer.process_streaming_tweets(status)
        if response["code"] == 200:
            gv.logger.info(response)
        elif response["code"] == 202:
            gv.logger.warning(response)
        else:
            gv.logger.error(response)