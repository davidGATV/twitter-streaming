from managers.twitter_streaming_manager import TwitterStreamingManager
from helper import global_variables as gv
from managers.elasticsearch_connector import ElasticsearchManager


class TwitterStreamingService:
    def __init__(self, languages: {list} = None, track: {list} = None, es_index: {str} = "default"):
        self.languages = languages
        self.track = track
        self.es_index = es_index
        if languages is not None and track is not None and es_index != "default":
            self.tw_stream_manager = TwitterStreamingManager(languages=languages,
                                                             track=track,
                                                             es_index=es_index)
        else:
            self.tw_stream_manager = None
        self.output = {"status": "", "status_code": 200}
        if gv.logger is None:
            gv.init_logger_object()

    def start_streaming(self, es_index: {str}):
        try:
            self.output = self.tw_stream_manager.start_streaming_process(thread_name=es_index)
        except Exception as e:
            gv.logger.error(e)
            self.output = {"status": str(e),
                           "status_code": 400}

    def stop_streaming(self, es_index: {str}):
        try:
            self.output = TwitterStreamingManager.stop_streaming_process(thread_name=es_index)
        except Exception as e:
            gv.logger.error(e)
            self.output = {"status": str(e),
                           "status_code": 400}

    @staticmethod
    def get_data_from_es(es: {ElasticsearchManager}, es_index: {str}):
        data = {"n_docs": -1, "status": "Stopped"}
        try:
            data["n_docs"] = es.es.count(index=es_index, doc_type="doc")
            thread_names = gv.get_thread_names()
            if thread_names:
                if es_index in gv.get_thread_names():
                    data["status"] = "Running"
        except Exception as e:
            gv.logger.error(e)
        return data