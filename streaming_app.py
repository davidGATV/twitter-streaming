from flask import Flask, render_template, request
import os
from helper import global_variables as gv
from services.twitter_streaming_service import TwitterStreamingService
from managers.elasticsearch_connector import ElasticsearchManager


# ===================================================================
app_title = os.getenv('APP_TITLE', 'Twitter Streaming Service')
os.environ["GLOBAL_PARENT_DIR"] = ""
base_url = "http://" + str(gv.host) + ":" + str(gv.port)
url_elastic_hq = "http://localhost:5000"
url_streaming_status = base_url + "/streaming/status"
url_streaming_form = base_url + "/streaming/form"
url_streaming_start = base_url + "/streaming/start"
url_streaming_stop = base_url + "/streaming/stop"
url_streaming_analytics = base_url + "/streaming/analytics"
# ===================================================================

app = Flask(__name__,
            static_folder=os.path.join('www', 'static'),
            template_folder=os.path.join('www', "templates"))
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('menu.html', app_title=app_title,
                           url_streaming_status=url_streaming_status,
                           url_streaming_form=url_streaming_form,
                           url_streaming_analytics=url_streaming_analytics)


@app.route('/streaming/form', methods=['GET', 'POST'])
def streaming_form():
    return render_template('form.html', app_title=app_title, url_streaming_start=url_streaming_start,
                           url_streaming_status=url_streaming_status,
                           url_streaming_form=url_streaming_form,
                           url_streaming_analytics=url_streaming_analytics,
                           url_streaming_home=base_url)


@app.route('/streaming/start', methods=['GET', 'POST'])
def streaming_start():
    # POST method indicates that we have data from the form
    if request.method == 'POST':
        es_index = request.form.get("database")
        languages = [request.form.get("languages")]
        keywords_request = request.form.get("keywords")
        search_words = [i.strip() for i in keywords_request.split(",")]

        gv.streaming_service = TwitterStreamingService(languages=languages,
                                                       track=search_words,
                                                       es_index=es_index)
        gv.streaming_service.start_streaming(es_index=es_index)

    data_es = [{"name": i} for i in gv.elasticsearch_manager.get_indexes() if not i.startswith(".")]
    return render_template('status.html', app_title=app_title, data_es=data_es,
                           url_streaming_status=url_streaming_status,
                           url_streaming_form=url_streaming_form,
                           url_streaming_analytics=url_streaming_analytics,
                           url_streaming_home=base_url)


@app.route('/streaming/stop/<string:es_index>', methods=['GET', 'POST'])
def streaming_stop(es_index: {str}):
    if gv.streaming_service is None:
        gv.streaming_service = TwitterStreamingService()
    gv.streaming_service.stop_streaming(es_index=es_index)
    data = TwitterStreamingService.get_data_from_es(es=gv.elasticsearch_manager, es_index=es_index)
    return render_template('monitoring_es.html', app_title=app_title, es_name=es_index,
                           n_docs=data["n_docs"]["count"],
                           status=data["status"],
                           url_streaming_form=url_streaming_form,
                           url_streaming_stop=url_streaming_stop,
                           url_streaming_status=url_streaming_status,
                           url_streaming_analytics=url_streaming_analytics,
                           url_streaming_home=base_url
                           )


@app.route('/streaming/status/', methods=['GET', 'POST'])
def streaming_status():
    # Retrieve index
    data_es = [{"name": i} for i in gv.elasticsearch_manager.get_indexes() if not i.startswith(".")]
    return render_template('status.html', app_title=app_title, data_es=data_es,
                           url_streaming_status=url_streaming_status,
                           url_streaming_form=url_streaming_form,
                           url_streaming_analytics=url_streaming_analytics,
                           url_streaming_home=base_url
                           )


@app.route('/streaming/status/<string:es_index>', methods=['GET'])
def get_status(es_index):
    data = TwitterStreamingService.get_data_from_es(es=gv.elasticsearch_manager, es_index=es_index)
    print(data)
    url_streaming_stop_monitoring = url_streaming_stop + "/" + es_index
    return render_template('monitoring_es.html', app_title=app_title, es_name=es_index,
                           n_docs=data["n_docs"]["count"],
                           status=data["status"],
                           url_streaming_form=url_streaming_form,
                           url_streaming_stop=url_streaming_stop_monitoring,
                           url_streaming_status=url_streaming_status,
                           url_streaming_analytics=url_streaming_analytics,
                           url_streaming_home=base_url
                           )


@app.route('/streaming/analytics', methods=['GET', 'POST'])
def streaming_monitoring():
    return render_template('analytics.html', app_title=app_title, url_elastic_hq=url_elastic_hq,
                           url_streaming_status=url_streaming_status,
                           url_streaming_form=url_streaming_form,
                           url_streaming_analytics=url_streaming_analytics,
                           url_streaming_home=base_url
                           )


if __name__ == '__main__':
    gv.init_logger_object()
    gv.init_threads()
    gv.elasticsearch_manager = ElasticsearchManager(host=gv.es_host,
                                                    port=gv.es_port)
    if not gv.elasticsearch_manager.connection:
        gv.elasticsearch_manager.connect()
    gv.streaming_service = None

    app.run(host=gv.host, port=gv.port, passthrough_errors=False, threaded=True)