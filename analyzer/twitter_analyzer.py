from helper import global_variables as gv
from textblob import Blobber
from textblob.sentiments import NaiveBayesAnalyzer
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from flair.models import TextClassifier
from flair.data import Sentence
from helper.twitter_utils import remove_url, parser_tweet
from tqdm import tqdm
import numpy as np


class TwitterAnalyzer:
    def __init__(self):
        self.blobber_model = None
        self.vader_model = None
        self.flair_model = None
        self.sentiment_model = gv.sentiment_model
        if gv.logger is None:
            gv.init_logger_object()

    def set_up_flair_model(self):
        try:
            self.flair_model = TextClassifier.load('en-sentiment')
        except Exception as e:
            gv.logger.error(e)

    def set_up_vader_model(self):
        try:
            self.vader_model = SentimentIntensityAnalyzer()
        except Exception as e:
            gv.logger.error(e)

    def set_up_blobber_model(self):
        try:
            self.blobber_model = Blobber(analyzer=NaiveBayesAnalyzer())
        except Exception as e:
            gv.logger.error(e)

    def select_model(self):
        try:
            # ===================================
            # Set up Models
            if self.sentiment_model == "vader":
                if self.vader_model is None:
                    self.set_up_vader_model()
            else:
                if self.flair_model is None:
                    self.set_up_flair_model()
            # ===================================
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def extract_data_from_tweets_iterator(data: {iter}):
        tweets_json = []
        try:
            gv.logger.info("Extracting data from Tweets...")
            tweets_json = [parser_tweet(tweet._json) for tweet in data]
            gv.logger.info("Done!")
        except Exception as e:
            gv.logger.error(e)
        return tweets_json

    def launch_sentiment_analysis(self, data: {list}):
        response = []
        try:
            # Clean data
            # Remove URLs
            tweets_processed = [remove_url(tweet['full_text']) for tweet in data]
            # Select Model for Sentiment Analysis
            self.select_model()

            # Retrieve subjectivity
            if self.blobber_model is None:
                self.set_up_blobber_model()

            # Retrieve sentiment and confidence (Polarity) and subjectivity
            with tqdm(total=len(tweets_processed)) as pbar:
                for i, tweet_text in enumerate(tweets_processed):
                    pbar.update(1)
                    if self.sentiment_model == "flair":
                        gv.logger.info("Sentiment Analysis using Flair!")
                        response += [self.flair_sentiment_analysis(tweet_text=tweet_text)]
                    else:
                        gv.logger.info("Sentiment Analysis using Vader!")
                        response += [self.vader_sentiment_analysis(tweet_text=tweet_text)]
        except Exception as e:
            gv.logger.error(e)
        return iter(response)

    def flair_sentiment_analysis(self, tweet_text: {str}):
        response = {}
        try:
            sentence = Sentence(tweet_text)
            self.flair_model.predict(sentence)
            label = sentence.labels[0]
            res_blobber = self.blobber_model(tweet_text)
            subjectivity = round(res_blobber.subjectivity,3)
            # Check Value
            if 0.4 <= label.score <= .65:
                sentiment = "neutral"
            else:
                sentiment = label.value.lower()
            response = {'sentiment': sentiment, 'confidence': round(label.score, 3),
                        'subjectivity': subjectivity}
        except Exception as e:
            gv.logger.error(e)
        return response

    def vader_sentiment_analysis(self, tweet_text: {str}):
        response = []
        try:
            sentiment_list = ["positive", "neutral", "negative"]
            res_blobber = self.blobber_model(tweet_text)
            subjectivity = round(res_blobber.subjectivity,3)
            scores = self.vader_model.polarity_scores(tweet_text)
            scores_list = [scores["pos"], scores["neu"], scores["neg"]]
            sentiment_argmax = np.argmax(scores_list)
            sentiment = sentiment_list[int(sentiment_argmax)]
            response = {'sentiment': sentiment,
                        'confidence': round(scores_list[int(sentiment_argmax)], 3),
                        'subjectivity': subjectivity}
        except Exception as e:
            gv.logger.error(e)
        return response

    def launch_user_clustering(self, data: {list}):
        response = []
        try:
            # Retrieve data from users
            user_data = [tweet["user"] for tweet in data]

            # TODO: Create a user embedding

            # TODO: Perform clustering


        except Exception as e:
            gv.logger.error(e)
        return iter(response)