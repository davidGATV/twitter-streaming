import re
from helper import global_variables as gv
from tweepy.models import Status


def remove_url(txt):
    """Replace URLs found in a text string with nothing
    (i.e. it will remove the URL from the string).

    Parameters
    ----------
    txt : string
        A text string that you want to parse and remove urls.

    Returns
    -------
    The same txt string with url's removed.
    """
    return " ".join(re.sub("([^0-9A-Za-z \t])|(\w+:\/\/\S+)", "", txt).split())


def convert_status_tweet_to_json(tweet: {Status}):
    response = {}
    try:
        response = tweet._json
    except Exception as e:
        gv.logger.error(e)
    return response


def parser_tweet(tweet: {dict}):
    response = {}
    try:
        tweet_required_columns = {"created_at", "id", "text", "entities", "source", "is_quote_status",
                                  "retweet_count", "favorite_count", "retweeted", "favorited",
                                  "lang", "user", "possibly_sensitive"}
        for k in tweet_required_columns:
            if k in tweet.keys():
                response[k] = tweet[k]
    except Exception as e:
        gv.logger.error(e)
    return response