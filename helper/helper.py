import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import itertools
import collections
import json
import nltk
from nltk import bigrams
from nltk.corpus import stopwords
from textblob import Blobber
from textblob.sentiments import NaiveBayesAnalyzer
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from tqdm import tqdm
import re
import os
import networkx as nx
import jsonpickle
import warnings
from helper import global_variables as gv
warnings.filterwarnings("ignore")

sns.set(font_scale=1.5)
sns.set_style("whitegrid")


def remove_url(txt):
    """Replace URLs found in a text string with nothing
    (i.e. it will remove the URL from the string).

    Parameters
    ----------
    txt : string
        A text string that you want to parse and remove urls.

    Returns
    -------
    The same txt string with url's removed.
    """

    return " ".join(re.sub("([^0-9A-Za-z \t])|(\w+:\/\/\S+)", "", txt).split())


def remove_collection_words(tweets_no_urls):
    try:
        # Create a sublist of lower case words for each tweet
        words_in_tweet = [tweet.lower().split() for tweet in tweets_no_urls]

        # Download stopwords
        nltk.download('stopwords')
        stop_words = set(stopwords.words('english'))

        # Remove stop words from each tweet list of words
        tweets_nsw = [[word for word in tweet_words if not word in stop_words]
                      for tweet_words in words_in_tweet]

        # Remove collection words
        collection_words = ['climatechange', 'climate', 'change']

        tweets_nsw_nc = [[w for w in word if not w in collection_words]
                         for word in tweets_nsw]
    except Exception as e:
        gv.logger.error(e)
        tweets_nsw_nc = None
    return tweets_nsw_nc


def compute_bigrams(tweets_nsw_nc):
    try:
        # Create list of lists containing bigrams in tweets
        terms_bigram = [list(bigrams(tweet)) for tweet in tweets_nsw_nc]
        # Flatten list of bigrams in clean tweets
        bigrams_ls = list(itertools.chain(*terms_bigram))

        # Create counter of words in clean bigrams
        bigram_counts = collections.Counter(bigrams_ls)

    except Exception as e:
        gv.logger.error(e)
        bigram_counts = None
    return bigram_counts


def generate_bigram_dataframe(bigram_counts):
    try:
        bigram_df = pd.DataFrame(bigram_counts.most_common(20),
                                 columns=['bigram', 'count'])

        bigram_df.head()
        bigram_df.to_csv('bigram_climate_change.csv')
    except Exception as e:
        gv.logger.error(e)
        bigram_df = None
    return bigram_df


def visualize_network_bigram(bigram_df):
    try:
        # Create dictionary of bigrams and their counts
        d = bigram_df.set_index('bigram').T.to_dict('records')
        # Create network plot
        G = nx.Graph()

        # Create connections between nodes
        for k, v in d[0].items():
            G.add_edge(k[0], k[1], weight=(v * 10))

        G.add_node("china", weight=100)
        fig, ax = plt.subplots(figsize=(10, 8))

        pos = nx.spring_layout(G, k=1)

        # Plot networks
        nx.draw_networkx(G, pos,
                         font_size=16,
                         width=3,
                         edge_color='grey',
                         node_color='purple',
                         with_labels=False,
                         ax=ax)

        # Create offset labels
        for key, value in pos.items():
            x, y = value[0] + .135, value[1] + .045
            ax.text(x, y,
                    s=key,
                    bbox=dict(facecolor='red', alpha=0.25),
                    horizontalalignment='center', fontsize=13)

        plt.show()
    except Exception as e:
        gv.logger.error(e)
    return


def get_sentiment(analysis):
    try:
        data = [analysis["neg"], analysis["pos"]]
        categories = ["neg", "pos"]
        if analysis["neg"] == analysis["pos"]:
            res = "neu"
        else:
            i = int(np.argmax(data))
            res = categories[i]
    except Exception as e:
        gv.logger.error(e)
        res = ""
    return res


def analyze_sentiments(tweets_no_urls):
    try:
        # Create textblob objects of the tweets
        tb = Blobber(analyzer=NaiveBayesAnalyzer())
        # TextBlob
        sentiment_objects = [tb(tweet) for tweet in tweets_no_urls]
        scores = []
        if gv.sa_model == "vader":
            print(11)
            analyser = SentimentIntensityAnalyzer()
            scores = [analyser.polarity_scores(tweet_v) for tweet_v in tweets_no_urls]
        # Vader Analysis
        sentiment_values = []
        gv.logger.warning("Sentiment analysis Processing ... ")
        with tqdm(total=len(sentiment_objects)) as pbar:
            for i, tweet in enumerate(sentiment_objects):
                pbar.update(1)
                if gv.sa_model != "vader":
                    data = [tweet.polarity, tweet.subjectivity, tweet.sentiment.classification,
                            tweet.sentiment.p_neg, tweet.sentiment.p_pos, tweet.raw]
                else:
                    data = [scores[i]['compound'], tweet.subjectivity, get_sentiment(scores[i]),
                            scores[i]['neg'], scores[i]['pos'], tweet.raw]
                sentiment_values.append(data)
        # Create dataframe containing the polarity value and tweet text
        sentiment_df = pd.DataFrame(sentiment_values, columns=["polarity", "subjectivity", "classification",
                                                               "p_neg", "p_pos", "tweet"])

        sentiment_df.head()
    except Exception as e:
        gv.logger.error(e)
        sentiment_df = None
    return sentiment_df


def visualize_sentiment_histogram(sentiment_df, fig_size=(8,6)):
    try:
        fig, ax = plt.subplots(figsize=fig_size)

        # Plot histogram with break at zero
        sentiment_df.hist(bins=[-1, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1],
                          ax=ax,
                          color="purple")

        plt.title("Sentiments from Tweets on Climate Change")
        plt.show()
    except Exception as e:
        gv.logger.error(e)
    return


def get_user_information(data):
    try:
        user_data = {"created_at": data['created_at'],
                     "description": data['description'],
                     "entities": data['entities'],
                     "favourites_count": data['favourites_count'],
                     "followers_count": data['followers_count'],
                     "friends_count": data['friends_count'],
                     "geo_enabled": data['geo_enabled'],
                     "id": data['id'],
                     "id_str": data['id_str'],
                     "lang": data['lang'],
                     "location": data['location'],
                     "name": data['name'],
                     "screen_name": data['screen_name'],
                     "profile_background_image_url": data['profile_background_image_url'],
                     "profile_image_url": data["profile_image_url"],
                     "statuses_count": data['statuses_count'],
                     "time_zone": data['time_zone'],
                     "url": data['url']
                     }
    except Exception as e:
        gv.logger.error(e)
        user_data = {}
    return user_data


def get_additional_information(tweet):
    try:
        additional_data = {}

        # Has hashtag
        if len(tweet['entities']['hashtags']) == 0:
            additional_data['has_hashtag'] = 0
        else:
            additional_data['has_hashtag'] = 1

        # Has url
        if len(tweet['entities']['urls']) == 0:
            additional_data['has_url'] = 0
        else:
            additional_data['has_url'] = 1

        # Is reply?
        if tweet['in_reply_to_status_id'] == None:
            additional_data['is_reply'] = 0
        else:
            additional_data['is_reply'] = 1
    except Exception as e:
        gv.logger.error(e)
        additional_data = {}
    return additional_data


def select_tweets_data(tweet):
    try:
        main_data = {"created_at": tweet['created_at'],
                     "entities": tweet['entities'],
                     "id": tweet['id'],
                     "id_str": tweet['id_str'],
                     "geo": tweet['geo'],
                     "place": tweet['place'],
                     "metadata": tweet['metadata'],
                     "source": tweet['source'],
                     "retweet_count": tweet['retweet_count'],
                     "favorite_count": tweet['favorite_count'],
                     "text": tweet['full_text'],
                     "user": get_user_information(tweet['user'])}
        additional_data = get_additional_information(tweet)
        data = {**main_data, **additional_data}

    except Exception as e:
        gv.logger.error(e)
        data = None
    return data


def get_save_tweets(filepath, tweets):
    try:
        tweetCount = 0
        processed_tweets = []
        gv.logger.info('Saving Tweets in json file. This process may last few minutes...')
        with open(filepath, 'w') as f:
            # Send the query
            for tweet in tweets:
                processed_tweet = select_tweets_data(tweet=tweet._json)
                processed_tweet_json = json.dumps(processed_tweet)
                f.write(processed_tweet_json)
                tweetCount += 1
                processed_tweets.append(processed_tweet)
            gv.logger.info("Downloaded {0} tweets".format(tweetCount))
    except Exception as e:
        gv.logger.error(e)
        processed_tweets = None
    return processed_tweets


def get_processed_tweets(tweets, min_tweets=10):
    try:
        tweetCount = 0
        processed_tweets = []
        with tqdm(total=len(range(tweets.limit))) as pbar:
            for tweet in tweets:
                pbar.update(1)
                processed_tweet = select_tweets_data(tweet=tweet._json)
                processed_tweets.append(processed_tweet)
                tweetCount +=1
        # Check count
        if tweetCount <= min_tweets:
            processed_tweets = None
    except Exception as e:
        gv.logger.error(e)
        processed_tweets = None
    return processed_tweets


def tweets_to_df(filepath):
    try:
        tweets = list(open(filepath, 'rt'))

        text = []
        weekday = []
        month = []
        year = []
        day = []
        hour = []
        hashtag = []
        url = []
        favorite = []
        reply = []
        retweet = []
        follower = []
        following = []
        user = []
        screen_name = []
        description = []
        coordinates = []
        for t in tweets:
            t = jsonpickle.decode(t)

            # Text
            text.append(t['text'])

            # Coordinates
            if t['coordinates'] is not None:
                coordinates.append(t['coordinates'])
            else:
                coordinates.append((-1,-1))
            # Decompose date
            date = t['created_at']
            weekday.append(date.split(' ')[0])
            month.append(date.split(' ')[1])
            day.append(date.split(' ')[2])
            year.append(date.split(' ')[5])
            time = date.split(' ')[3].split(':')
            hour.append(time[0])

            # Has hashtag
            if len(t['entities']['hashtags']) == 0:
                hashtag.append(0)
            else:
                hashtag.append(1)

            # Has url
            if len(t['entities']['urls']) == 0:
                url.append(0)
            else:
                url.append(1)

            # Number of favs
            favorite.append(t['favorite_count'])

            # Is reply?
            if t['in_reply_to_status_id'] == None:
                reply.append(0)
            else:
                reply.append(1)

                # Retweets count
            retweet.append(t['retweet_count'])

            # Followers number
            follower.append(t['user']['followers_count'])

            # Following number
            following.append(t['user']['friends_count'])

            # Add user
            user.append(t['user']['name'])

            # Add user description
            description.append(t['user']['description'])

            # Add screen name
            screen_name.append(t['user']['screen_name'])

        d = {'text': text,
             'weekday': weekday,
             'month': month,
             'year':year,
             'day': day,
             'hour': hour,
             'has_hashtag': hashtag,
             'has_url': url,
             'fav_count': favorite,
             'is_reply': reply,
             'retweet_count': retweet,
             'followers': follower,
             'following': following,
             'coordinates':coordinates,
             'user': user,
             'screen_name': screen_name,
             'user_description':description
             }
    except Exception as e:
        gv.logger.error(e)
        d = None
    return pd.DataFrame(data=d)


def extract_tweets_json(filename):
    try:
        tweets_file = list(open(filename, 'rt'))
        tweets = [jsonpickle.decode(t) for t in tweets_file]
    except Exception as e:
        gv.logger.error(e)
        tweets = None
    return tweets


def prepare_directory(dir_path):
    try:
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
    except Exception as e:
        gv.logger.error(e)
    return

def check_search_items(search_terms):
    try:
        new_search_item = []
        hashtag = "#"

        # Add Hashtag
        for search_item in search_terms:
            if not hashtag in search_item:
                new_search_item.append(hashtag + search_item)
            else:
                new_search_item.append(search_item)
    except Exception as e:
        gv.logger.error(e)
        new_search_item = search_terms
    return new_search_item