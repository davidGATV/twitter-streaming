@ECHO OFF
ECHO ============================
ECHO Starting ElasticsearchHQ server ...
ECHO ============================
cd %Elasticsearch_HQ%
call conda activate TWITTER_ANALYSIS
python application.py
