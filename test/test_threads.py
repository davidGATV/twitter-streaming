import logging
import time
from streaming.streaming_thread import StreamingThread

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

def worker():
    logging.debug('Starting')
    time.sleep(20)
    logging.debug('Exiting')


def my_service():
    logging.debug('Starting')
    time.sleep(3)
    logging.debug('Exiting')

list_threads = []
t = StreamingThread(name='my_service', target=my_service)
list_threads.append(t)
w = StreamingThread(name='worker', target=worker)
list_threads.append(w)
w2 = StreamingThread(name="worker2", target=worker)
list_threads.append(w2)
thread_names = [i.name for i in list_threads]

w.start()
print(list_threads[1]._started._flag)


w2.start()
print(list_threads[2]._started._flag)
t.start()


# =======================
# stop
w.start()
w.kill()
w.join()
if not w.isAlive():
  print('thread killed')


