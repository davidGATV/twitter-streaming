from services.twitter_streaming_service import TwitterStreamingService


if __name__ == '__main__':
    search_words = ["COVID-19", "Coronavirus"]
    languages = ["en"]
    service = TwitterStreamingService(languages=languages, track=search_words,es_index="second_database")
    service.start_streaming(es_index="second_database")
    print(service.output)