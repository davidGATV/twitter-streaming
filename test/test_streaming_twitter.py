import sys
import os
import tweepy as tw
if "test" in os.getcwd():
    sys.path.append('../')
    os.environ["GLOBAL_PARENT_DIR"] = ".."

else:
    os.environ["GLOBAL_PARENT_DIR"] = ".."

from managers.twitter_connector import TwitterConnector
from streaming.twitter_streaming_process import TwitterStreamListener
from helper import global_variables as gv


def test_streaming(languages: {list}, track: {list}):
    try:
        tw_connector = TwitterConnector()
        tw_connector.set_up_connection()

        api = tw_connector.api
        stream_listener = TwitterStreamListener()
        stream = tw.Stream(auth=api.auth, listener=stream_listener)
        stream.filter(languages=languages,
                      track=track)
    except Exception as e:
        gv.logger.error(e)


if __name__ == '__main__':
    if gv.logger is None:
        gv.init_logger_object()

    search_words = ["COVID-19", "Coronavirus"]
    lang = "en"
    test_streaming(languages=[lang], track=search_words)
