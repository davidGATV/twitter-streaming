$(document).ready(function() {
    $("#success-alert").hide();
  validate();
  $('input').on('keyup', validate);

});

function validate() {
  var inputsWithValues = 0;
  
  // get all input fields except for type='submit'
  var myInputs = $("input:not([type='submit'])");
  console.log(myInputs)
  myInputs.each(function(e) {
    // if it has a value, increment the counter
    if ($(this).val()) {
      inputsWithValues += 1;
    }
  });

  if (inputsWithValues == myInputs.length) {
    $('#btnFetch').prop("disabled", false);
  } else {
    $('#btnFetch').prop("disabled", true);
  }
}

function loading(){
    var spinner = document.getElementById('spnLoader');
    console.log(spinner);
    var content = document.getElementById('content');
    console.log(content);
    spinner.style.display = "block";
    content.style.display = "none";
}

function formAction(){
    // Current value
    var db_value = $('#pickerStatus').val();
    console.log(db_value);
    var form_action = document.myStatusForm.action
    console.log(form_action);
    var response_action = form_action.concat("/").concat(db_value)
    console.log(response_action)
    // new action
    document.myStatusForm.action = response_action;
}

function checkStatus(){
    // Check if the variable
    var status_name = String(statusProcess);
    console.log(status_name)

    if (status_name == 'Running')
    {
        $('#btnStop').prop("disabled", false);
        $('#btnStart').prop("disabled", true);
    }else{
        $('#btnStop').prop("disabled", true);
        $('#btnStart').prop("disabled", false);
    }
}


function formStopStreaming(){
    // Current value
    var es_index = String(es_index)
    console.log(es_index);
    var form_action = document.monitoringForm.action
    console.log(form_action);
    var response_action = form_action.concat("/").concat(db_value)
    console.log(response_action)
    // new action
    document.monitoringForm.action = response_action;
}

function showAlert()
{   var alert = $("#success-alert")
    console.log(alert)
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
    $("#success-alert").slideUp(500);
    });
};