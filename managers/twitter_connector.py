import tweepy as tw
from tweepy import API, StreamListener
from tweepy.models import Status
from helper import global_variables as gv


class TwitterConnector:
    def __init__(self):
        self.consumer_key = gv.CONSUMER_KEY
        self.consumer_secret = gv.CONSUMER_SECRET
        self.access_token = gv.ACCESS_TOKEN
        self.access_token_secret = gv.ACCESS_TOKEN_SECRET
        self.api = None
        if gv.logger is None:
            gv.init_logger_object()

    def set_up_connection(self):
        try:
            gv.logger.info("Connecting to Twitter API ... ")
            auth = tw.OAuthHandler(self.consumer_key, self.consumer_secret)
            auth.set_access_token(self.access_token, self.access_token_secret)
            self.api = API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True,
                           retry_count=10, retry_delay=5, retry_errors=5)
        except Exception as e:
            gv.logger.error(e)

    @staticmethod
    def tweet_gathering(api: {API}, search_words: {str}, date_since: {str},
                        items: {int} = 1000, lang: {str} = 'en'):
        tweets = None
        try:
            gv.logger.info("Retrieving Tweets ... ")
            # Collect tweets
            tweets = tw.Cursor(api.search,
                               lang=lang,
                               q=(search_words),
                               since=date_since,
                               include_entities=True,
                               monitor_rate_limit=True,
                               wait_on_rate_limit=True,
                               tweet_mode='extended').items(items)
        except Exception as e:
            gv.logger.error(e)
        return tweets


