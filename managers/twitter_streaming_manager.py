import tweepy as tw
from managers.twitter_connector import TwitterConnector
from streaming.twitter_streaming_process import TwitterStreamListener
from streaming.streaming_thread import StreamingThread
from helper import global_variables as gv
from urllib3.exceptions import ProtocolError


class TwitterStreamingManager:
    def __init__(self, languages: {list}, track: {list}, es_index: {str}):
        self.languages = languages
        self.track = track
        self.twitter_connector = TwitterConnector()
        self.stream_listener = TwitterStreamListener(es_index=es_index)
        self.stream = None
        if gv.logger is None:
            gv.init_logger_object()

    def run_streaming(self):
        try:
            # Start connection to Twitter
            if self.twitter_connector.api is None:
                self.twitter_connector.set_up_connection()
            api = self.twitter_connector.api

            # Start streaming
            self.stream = tw.Stream(auth=api.auth, listener=self.stream_listener)
            while True:
                try:
                    self.stream.filter(languages=self.languages, track=self.track,
                                       stall_warnings=True)
                except (ProtocolError, AttributeError):
                    continue
            self.stream.filter(languages=self.languages, track=self.track)
        except Exception as e:
            gv.logger.error(e)

    def start_streaming_process(self, thread_name: {str}):
        response = {"status": "Streaming Process running!", "status_code": 200}
        try:
            add_thread = True
            # If the list of threads is not empty
            if gv.stream_threads:
                # Check if there is a thread with the same name running
                thread_names = [i.name for i in gv.stream_threads]
                # Thread with the same name
                if thread_name in thread_names:
                    thread_idx = thread_names.index(thread_name)
                    # If it is still alive
                    if gv.stream_threads[thread_idx].is_alive():
                        add_thread = False
                        response = {"status": "A Streaming Process is already running", "status_code": 202}
                    else:
                        # remove old thread from list
                        gv.stream_threads.pop(thread_idx)
            if add_thread:
                streaming_thread = __class__.create_new_streaming_thread(thread_name=thread_name,
                                                                         target_func=self.run_streaming)
                # Save thread into list
                gv.stream_threads.append(streaming_thread)
        except Exception as e:
            gv.logger.error(e)
            response = {"status": str(e), "status_code": 400}
        return response

    @staticmethod
    def create_new_streaming_thread(thread_name: {str}, target_func):
        streaming_thread = None
        try:
            # Create new thread
            streaming_thread = StreamingThread(name=thread_name, target=target_func)
            # Start thread process
            streaming_thread.start()
            gv.logger.warning("Thread %s has started!", str(streaming_thread.name))
        except Exception as e:
            gv.logger.error(e)
        return streaming_thread

    @staticmethod
    def kill_streaming_thread(streaming_thread: {StreamingThread}):
        response = False
        try:
            # Still alive then kill it
            if streaming_thread.is_alive():
                streaming_thread.kill()
                streaming_thread.join()
                response = True
                gv.logger.warning("Thread %s was killed!", str(streaming_thread.name))
        except Exception as e:
            gv.logger.error(e)
        return response

    @staticmethod
    def stop_streaming_process(thread_name: {str}):
        response = {"status": "Streaming Process stopped",
                    "status_code": 200}
        try:
            not_found = True
            # If the list of threads is not empty
            if gv.stream_threads:
                # Check if there is a thread with the same name running
                thread_names = [i.name for i in gv.stream_threads]
                # Thread with the same name
                if thread_name in thread_names:
                    thread_idx = thread_names.index(thread_name)
                    # Stop selected thread
                    current_thread = gv.stream_threads[thread_idx]
                    response = __class__.kill_streaming_thread(streaming_thread=current_thread)
                    if response:
                        # Remove from list
                        gv.stream_threads.pop(thread_idx)
                        not_found = False
            # Check not found
            if not_found:
                response = {"status": "Thread Process not found",
                            "status_code": 304}
        except Exception as e:
            gv.logger.error(e)
            response = {"status": str(e), "status_code": 400}
        return response